import path from "path";
import { Configuration } from "webpack";
const CopyWebpackPlugin = require('copy-webpack-plugin');

const process = {
	env: {
		SERVER_URL: 'http://192.168.2.162:3000',
		SERVER_PROXY: '/api',
		WEBPACK_PORT: 8080
	}
}

const config: Configuration = {
  entry: "./app/app.tsx",
  plugins: [
    new CopyWebpackPlugin({
      patterns: [
          { from: 'app' }
      ]
  })
  ],
  module: {
    rules: [
      {
        test: /\.(ts|js)x?$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader",
          options: {
            presets: [
              "@babel/preset-env",
              "@babel/preset-react",
              "@babel/preset-typescript",
            ],
          },
        },
      },
    ],
  },
  resolve: {
    fallback: {
      'fs': false,
      'path': false, // ammo.js seems to also use path
    },
    extensions: [".tsx", ".ts", ".js"],
  },
  output: {
    path: path.resolve(__dirname, "dist"),
    filename: "bundle.js",
  },
  devServer: {
    static: path.join(__dirname, "dist"),
    compress: true,
    port: process.env.WEBPACK_PORT,
    proxy: {
      [process.env.SERVER_PROXY]: {
        secure: false,
        changeOrigin: true,
        target: process.env.SERVER_URL,
        pathRewrite: { [process.env.SERVER_PROXY]: '' },
      }
    }
  },
};

export default config;