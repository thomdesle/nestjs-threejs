import Ammo from 'ammojs-typed';
import { Clock } from "three";
import { PhysicsMachine } from './PhysicsMachine';

export class StateMachine {
    private callbacks: Array<(delta: number) => void> = [];
    private clock = new Clock();
    private _PhysicsMachine?: PhysicsMachine;

    constructor(callback: (StateMachine: StateMachine) => void) {
        Ammo().then(Ammo => {
            this._PhysicsMachine = new PhysicsMachine(Ammo);
            callback(this);
            this.frame();
        });
    }

    private frame = () => {
        requestAnimationFrame(this.frame);
        const delta = this.clock.getDelta();
        this.PhysicsMachine.update(delta);
        this.callbacks.map(cb => cb(delta));
    }
    
    public register = (cb: (delta: number) => void) => {
        this.callbacks.push(cb);
        return this;
    }

    public unregister = (cb: (delta: number) => void) => {
        const index = this.callbacks.findIndex(c => c === cb);
        if (index !== -1)
            this.callbacks.splice(index, 1);

        return this;
    }

    // force type since we can safely say we do everything after StateMachine is ready
    public get PhysicsMachine(): PhysicsMachine {
        return this._PhysicsMachine as any;
    }
}