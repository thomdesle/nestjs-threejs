import { Quaternion } from "three";
import Entity from "../Entity";

export default class EntityState {
    protected _rotationDirection?: Quaternion;
    protected _movementDirection?: Quaternion;
	protected _movementSpeed = .2;
	protected _rotationSpeed = .2;
    protected entity: Entity;

    constructor(entity: Entity) {
        this.entity = entity;
        window.StateMachine.register(this.update);
    }

    public startMoving = (direction: Quaternion, speed = .2) => {
        this._movementDirection = direction;
		this._movementSpeed = speed;
    }

    public stopMoving = () => {
        this._movementDirection = undefined;
    }

    public rotate = (quaternation: Quaternion, speed = .2) => {
        this._rotationDirection = quaternation;
		this._rotationSpeed = speed;
    }

    public update = (delta: number) => {
        const { _movementDirection, _rotationDirection, _rotationSpeed, _movementSpeed } = this;
        const { offsetPosition, setRotation, object3d } = this.entity;
        
        if (_movementDirection)
            offsetPosition(_movementDirection, _movementSpeed);

        if (_rotationDirection) {
            const rotation = object3d.quaternion.clone();
			if (_rotationDirection.equals(rotation)) {
				this._rotationDirection = undefined;
			} else {
				rotation.rotateTowards(_rotationDirection, _rotationSpeed);
				setRotation(rotation.x, rotation.y, rotation.z, rotation.w);
			}
        }
    }

    get rotationDirection() {
        return this._rotationDirection;
    }

    get movementDirection() {
        return this._movementDirection;
    }

	get movementSpeed(): number {
		return this._movementSpeed;
	}

	get rotationSpeed(): number {
		return this._rotationSpeed;
	}
}