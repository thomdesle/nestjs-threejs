import { Object3D, Quaternion, Vector3 } from "three"
import { SlotTypeEnum } from "../../models/asset/SlotTypeEnum";
import { mapSlots } from "../../utils/object3d/mapSlots";
import PhysicsBody from "../physics/PhysicsBody";

export default class Entity {
    readonly object3d: Object3D;
    readonly physicsBody: PhysicsBody;
    readonly slots: { [key in keyof typeof SlotTypeEnum]: Object3D };
    
    constructor(object3d: Object3D) {
        this.object3d = object3d;
        this.physicsBody = new PhysicsBody(object3d);
        this.slots = mapSlots(object3d);
    }

    public setScale = (scale: Vector3) => {
        this.object3d.scale.set(scale.x, scale.y, scale.z);
        this.physicsBody.uppdateCollisionShape();
    }
    
    public setCollisionShape = (shape: Vector3) => this.physicsBody.uppdateCollisionShape(shape);
    public setMass = (mass: number) => this.physicsBody.setMass(mass);
    public setPosition = (x: number, y: number, z: number) => this.physicsBody.setPosition(x, y, z);
    public offsetPosition = (quaternation: Quaternion, distance: number) => this.physicsBody.offsetPosition(quaternation, distance);
    public setRotation = (x: number, y: number, z: number, w: number) => this.physicsBody.setRotation(x, y, z, w);
}