import { Object3D, Quaternion } from "three";
import Animator from "../animator/Animator";
import Entity from "./Entity";
import EntityState from "./state/EntityState";

export default class LivingEntity extends Entity {
    private state;
	readonly animator;

    constructor(object3d: Object3D) {
        super(object3d);
        this.state = new EntityState(this);
		this.animator = new Animator(object3d);
    }

    get isMoving() {
        return !this.state.movementDirection;
    }

    public startMoving = (direction: Quaternion) => this.state.startMoving(direction);
    public stopMoving = () => this.state.stopMoving();
    public rotate = (quaternion: Quaternion, speed?: number) => {
		this.state.rotate(quaternion, speed);
	}
}