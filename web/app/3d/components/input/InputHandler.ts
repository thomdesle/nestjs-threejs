type IKeyConfig = {
    key: string
    disallowSimultaneously?: string[],
}

export default class InputHandler {
    private keyConfigs: IKeyConfig[] = [];
    private _keysPressed: string[] = [];
    readonly onKeyUp = (e?: KeyboardEvent) => {}
    readonly onKeyDown = (e: KeyboardEvent) => {}

    constructor(keys: IKeyConfig[] = [], onKeyUp: InputHandler['onKeyUp'], onKeyDown: InputHandler['onKeyDown']) {
        this.keyConfigs = keys;
        this.onKeyUp = onKeyUp;
        this.onKeyDown = onKeyDown;
        addEventListener('keydown', this._onKeyDown);
        addEventListener('keyup', this._onKeyUp)
        document.addEventListener('visibilitychange', this._onLoseFocus);
    }

    get keysPressed() {
        return this._keysPressed;
    }

    private _onKeyUp = (e: KeyboardEvent) => {
        const index = this.keysPressed.indexOf(e.key.toLowerCase());
        if (index === -1)
            return;

        this._keysPressed.splice(index, 1);
        this.onKeyUp(e);
    }

    private _onKeyDown = (e: KeyboardEvent) => {
        const key = e.key.toLowerCase();

        if (e.repeat)
            return;

        if (!this.keyConfigs.find(o => o.key === key))
            return;

        if (this.keysPressed.indexOf(key) === -1)
            this._keysPressed.push(key);
        
        const config = this.keyConfigs.find(o => o.key === key);
        if (config?.disallowSimultaneously?.length)
            this._keysPressed = this.keysPressed.map(keyPressed => {
                if (config.disallowSimultaneously?.indexOf(keyPressed) !== -1)
                    return;
                return keyPressed;
            }).filter(Boolean) as string[];
        
        this.onKeyDown(e);
    }

    private _onLoseFocus = () => {
        this._keysPressed = [];
        this.onKeyUp();
    }

    destroy = () => {
        removeEventListener('keydown', this._onKeyDown);
        removeEventListener('keyup', this._onKeyUp);
        document.removeEventListener("visibilitychange", this._onLoseFocus);
    }
}

export enum DirectionEnum {
    UP = Math.PI * 2,
    DIAGONAL_RIGHT_UP = Math.PI * 1.75,
    RIGHT = Math.PI * 1.5,
    DIAGONAL_RIGHT_DOWN = Math.PI * 1.25,
    DOWN = Math.PI,
    DIAGONAL_LEFT_DOWN = Math.PI * .75,
    LEFT = Math.PI * .5,
    DIAGONAL_LEFT_UP = Math.PI * .25
}

export const getDirectionFromKeyInput = (keysPressed: string[], map: [string, string, string, string] = ['w', 'a', 's', 'd']) => {
    let direction: DirectionEnum | undefined = undefined;
    const w = map[0].toLowerCase(), a = map[1].toLowerCase(), s = map[2].toLowerCase(), d = map[3].toLowerCase();

    const isHeld = (key: string) => keysPressed.indexOf(key) !== -1;

    if (isHeld(w)) direction = DirectionEnum.UP
    else if (isHeld(s)) direction = DirectionEnum.DOWN

    if (isHeld(a))
        if (direction !== undefined) direction = direction === DirectionEnum.UP 
            ? DirectionEnum.DIAGONAL_LEFT_UP 
            : DirectionEnum.DIAGONAL_LEFT_DOWN
        else direction = DirectionEnum.LEFT
    else if (isHeld(d))
        if (direction !== undefined) direction = direction === DirectionEnum.UP 
            ? DirectionEnum.DIAGONAL_RIGHT_UP 
            : DirectionEnum.DIAGONAL_RIGHT_DOWN
        else direction = DirectionEnum.RIGHT
        
    return direction;
}