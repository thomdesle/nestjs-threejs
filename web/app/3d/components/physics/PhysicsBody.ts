import Ammo from "ammojs-typed";
import { Object3D, Quaternion, Vector3 } from "three";
import { getShapeSize } from "../../utils/object3d/getShapeSize";

const DEFAULT_MARGIN = .05;
const DEFAULT_MASS = 1;

export default class PhysicsBody {
    readonly body: Ammo.btRigidBody
    readonly object3d: Object3D;

    constructor(object3d: Object3D) {
        this.object3d = object3d;
        this.body = this.createRigidBody();
        window.StateMachine.PhysicsMachine.register(this);
    }

    public offsetPosition = (quaternation: Quaternion, distance: number) => {
        const direction = new Vector3(1, 0, 0).normalize();
        direction.applyQuaternion(quaternation).multiply(new Vector3(distance, distance, distance))
        const { x, y, z } = this.getPosition().add(direction);
        this.setPosition(x, y, z);
    }

    public setPosition = (x: number, y: number, z: number) => {
        const { Ammo } = window.StateMachine.PhysicsMachine;
        const transform = this.body.getWorldTransform();
        transform.setOrigin(new Ammo.btVector3(x, y, z));
        this.body.setWorldTransform(transform);
        this.body.activate();
    }

    public setRotation = (x: number, y: number, z: number, w: number) => {
        const { Ammo } = window.StateMachine.PhysicsMachine;
        const transform = this.body.getWorldTransform();
        transform.setRotation(new Ammo.btQuaternion(x, y, z, w));
        this.body.setWorldTransform(transform);
        this.body.activate();
    }

    public setMass = (mass: number, inertia = new Vector3()) => {
        const { Ammo } = window.StateMachine.PhysicsMachine;
        const { x, y, z } = inertia;
        this.body.setMassProps(mass, new Ammo.btVector3(x, y, z));
        this.body.activate();
    }

    public getPosition = () => {
       const transform = this.body.getWorldTransform();
       const position = transform.getOrigin();
       return new Vector3(position.x(), position.y(), position.z())
    }

    public getRotation = () => {
        const { x, y, z, w } = this.body.getWorldTransform().getRotation();
        return new Quaternion(x(), y(), z(), w());
    }

    public uppdateCollisionShape = (size?: Vector3) => {
        this.body.setCollisionShape(this.createCollisionShape(size));
    }

    private createRigidBody = () => {
        const { Ammo } = window.StateMachine.PhysicsMachine;
        const { position, quaternion } = this.object3d;
        let transform = new Ammo.btTransform();
        transform.setIdentity();
        transform.setOrigin(new Ammo.btVector3(position.x, position.y, position.z));
        transform.setRotation(new Ammo.btQuaternion(quaternion.x, quaternion.y, quaternion.z, quaternion.w));

        const bodyInfo = new Ammo.btRigidBodyConstructionInfo(
            DEFAULT_MASS,
            new Ammo.btDefaultMotionState(transform),
            this.createCollisionShape(),
            new Ammo.btVector3()
        )

        return new Ammo.btRigidBody(bodyInfo);
    }

    private createCollisionShape = ({ x, y, z } = getShapeSize(this.object3d)) => {
        const { Ammo } = window.StateMachine.PhysicsMachine;
        const vector = new Ammo.btVector3(x*.5, y*.5, z*.5);
        const shape = new Ammo.btBoxShape(vector);
        shape.setMargin(DEFAULT_MARGIN);
        return shape;
    }

    public update = () => {
        let transform = this.body.getWorldTransform();
        let position = transform.getOrigin();
        let rotation = transform.getRotation();
        this.object3d.quaternion.set(rotation.x(), rotation.y(), rotation.z(), rotation.w());
        this.object3d.position.set(position.x(), position.y(), position.z());
    }
}