import { AnimationAction, AnimationClip, AnimationMixer, Object3D } from "three";

const DEFAULT_FADE_DURATION = .2;

export default class Animator {
    private mixer?: AnimationMixer;
    private activeClip?: AnimationClip;
    private activeAction?: AnimationAction;

    constructor(target?: Object3D) {
        if (target)
            this.setTarget(target);

        window.StateMachine.register((delta) => {
            this.mixer?.update(delta);
        })
    }

    public setTarget = (target: Object3D) => {
        if (this.mixer)
            this.mixer.stopAllAction();

        this.mixer = new AnimationMixer(target);
        return this;
    }

    public getMixer = () => this.mixer;

    public play = (clip: AnimationClip, speed?: number) => {
		if (!this.mixer)
			return;

        if (this.activeClip?.name === clip.name) {
			return;
		}

        const prev = this.activeAction;
        const next = this.mixer?.clipAction(clip).reset();
		if (speed)
			next.setDuration(speed);
			
        this.activeClip = clip;

        if (!!prev)
            this.activeAction = next.crossFadeFrom(prev, DEFAULT_FADE_DURATION, true).play();
        else 
            this.activeAction = next.play();
            
        return this.activeAction;
    }
}