import { Mesh, Object3D, Vector3 } from "three";
import { GLTF, GLTFLoader } from "three/examples/jsm/loaders/GLTFLoader";
import { FBXLoader } from "three/examples/jsm/loaders/FBXLoader";
import { AssetTypeEnum, isFBXAsset, isGLTFAsset, isMeshAsset } from "../../models/asset/AssetTypeEnum";
import { SlotTypeEnum } from "../../models/asset/SlotTypeEnum";
import { IAssetFile } from "../../models/asset/IAssetFile";

export type IAssetProps<T extends AssetTypeEnum> = {
    rotation?: Vector3
    position?: Vector3
    scale?: Vector3
    slots?: { [key: string]: SlotTypeEnum }
} & (
    T extends AssetTypeEnum.FBX ? {
        fbx: string
    } : T extends AssetTypeEnum.GLTF ? {
        gltf: string;
    } : T extends AssetTypeEnum.MESH ? {
        mesh: () => Mesh
    } : {}
);

export default class Asset<T extends AssetTypeEnum> {
    private props: IAssetProps<T>;

    constructor(props: IAssetProps<T>) {
        this.props = props;
    }

    private applyProps = (object3d: Object3D) => {
        const { 
            rotation = new Vector3(0, 0, 0), 
            position = new Vector3(0, 0, 0),
            scale = new Vector3(1, 1, 1)
        } = this.props;

        object3d.rotation.set(rotation.x, rotation.y, rotation.z);
        object3d.position.set(position.x, position.y, position.z);
        object3d.scale.set(scale.x, scale.y, scale.z);

        // nesting the object3d in a new one, to secure transforms and prevent interference with physics from ammo.js
        return new Object3D().add(object3d);
    }

    public getObjectSlots = (object3d: Object3D): { [key in keyof typeof SlotTypeEnum]: Object3D } => {
        const mappedSlots: { [key: string]: Object3D } = {};
        const slots = this.props.slots;
        if (!slots || !Object.keys(slots).length) 
            return {} as any;

        object3d.traverse((o: Object3D) => {
            if (slots[o.name]) {
                mappedSlots[slots[o.name]] = o;
            }
        });

        return mappedSlots as any;
    }

    // gets a new instance of the asset
    public get = (cb: (object3d: Object3D, file: IAssetFile<T>) => void) => {
        this.load((object3d, file) => {
            object3d = this.applyProps(object3d);
            cb(object3d, file);
        });
    }

    private load = (cb: (object3d: Object3D, file: IAssetFile<T>) => void) => {
        const { props, onSuccess, onError, onProgress } = this;
        if (isGLTFAsset(props))
            new GLTFLoader().load(props.gltf, (file: GLTF) => {
                cb(file.scene as Object3D, file as IAssetFile<T>);
                onSuccess();
            }, onProgress, onError);
        else if (isFBXAsset(props))
            new FBXLoader().load(props.fbx, (file?: Object3D) => {
                cb(file as Object3D, file as IAssetFile<T>);
                onSuccess();
            }, onProgress, onError);
        else if (isMeshAsset(props)) {
            const mesh = props.mesh();
            cb(mesh as Object3D, mesh as IAssetFile<T>);
            onSuccess();
        }
    }

    private onSuccess = () => {}
    private onProgress = () => {}
    private onError = (e: ErrorEvent) => console.log(e)
}