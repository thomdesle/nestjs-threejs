import { PerspectiveCamera, PointLight, Quaternion, Scene, Vector3, WebGLRenderer } from "three";
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls.js';
import { randomColor } from "../helpers/colors/randomColor";
import Asset from "./components/asset/Asset";
import LivingEntity from "./components/entity/LivingEntity";
import { AssetTypeEnum } from "./models/asset/AssetTypeEnum";
import { StateMachine } from "./StateMachine";

declare global {
	interface Window {
		StateMachine: StateMachine
	}
}

const ASSETS = {
	character: new Asset<AssetTypeEnum.GLTF>({
		gltf: '/3d/assets/character.glb',
	}),
	pistol: new Asset<AssetTypeEnum.GLTF>({
		gltf: '/3d/assets/pistol.glb',
		rotation: new Vector3(2.8, .5, 2)
	})
}


const scene = new Scene();
const camera = new PerspectiveCamera();
const renderer = new WebGLRenderer({ alpha: true });
const controls = new OrbitControls(camera, renderer.domElement);

window.StateMachine = new StateMachine(() => {
	initialize();
}).register((delta) => {
	controls.update();
	renderer.render(scene, camera);
});

const initialize = () => {
	document.body.appendChild(renderer.domElement);
	renderer.setSize(innerWidth, innerHeight);
	camera.aspect = innerWidth / innerHeight;
	renderer.setPixelRatio(devicePixelRatio * 1.5);
	camera.updateProjectionMatrix();
	scene.add(camera);
	camera.position.set(10, 10, 0);

	const light = new PointLight('black', 20);
	light.position.set(0, 10, 0);
	scene.add(light);

	if (!window.SpotifyListener)
		return;

	const { beats, bars, sections } = window.SpotifyListener?.intervalManagers;

	beats.listen(beat => {
		light.color.set(randomColor())
	});

	ASSETS.character.get((object3d, file) => {
		const entity = new LivingEntity(object3d);
		entity.setMass(0);
		entity.setPosition(0, 0, 0);
		ASSETS.pistol.get((object3d) => entity.slots.LEFT_HAND.add(object3d));
		scene.add(object3d);
		document.body.style.background = randomColor();

		bars.listen(bar => {
			const y = (window as any).y;
			const clip = file.animations[(window as any).x ? 20 : 23];
			(window as any).x = !(window as any).x;
			entity.animator.play(clip, y);
		});

		sections.listen(section => {
			if (section.duration < 10)
				return;

			(window as any).y = section.tempo / 100;

			document.body.style.background = randomColor();
			entity.rotate(new Quaternion().setFromAxisAngle(new Vector3(0, 1, 0), Math.random() * Math.PI * 10), section.duration / 1000);
		})
	});
}