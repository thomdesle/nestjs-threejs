import { Box3, Object3D, Vector3 } from "three";

export const getShapeSize = (object3d: Object3D) => {
    const size = new Vector3();
    new Box3().setFromObject(object3d).getSize(size);
    return size;
}