import { Object3D } from "three";
import { SlotTypeEnum } from "../../models/asset/SlotTypeEnum";

const SLOTS_MAPPING = {
    'Ctrl_Index2_Left': SlotTypeEnum.LEFT_HAND,
    'Ctrl_Index2_Right': SlotTypeEnum.RIGHT_HAND,
} as {[key: string]: SlotTypeEnum};

export const mapSlots = (object3d: Object3D): { [key in keyof typeof SlotTypeEnum]: Object3D } => {
    const slots: { [key in keyof typeof SlotTypeEnum]: Object3D } = {} as any;
    object3d.traverse(o => {
        if (SLOTS_MAPPING[o.name])
            // @ts-expect-error
            slots[SLOTS_MAPPING[o.name]] = o;
    });

    return slots;
}