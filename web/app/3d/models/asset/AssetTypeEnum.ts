import { IAssetProps } from "../../components/asset/Asset";

export enum AssetTypeEnum {
    GLTF = 'GLTF',
    FBX = 'FBX',
    MESH = 'MESH'
}

export const isGLTFAsset = (props: IAssetProps<any>): props is IAssetProps<AssetTypeEnum.GLTF> => 
    !!(props as IAssetProps<AssetTypeEnum.GLTF>).gltf;

export const isFBXAsset = (props: IAssetProps<any>): props is IAssetProps<AssetTypeEnum.FBX> => 
    !!(props as IAssetProps<AssetTypeEnum.FBX>).fbx;

export const isMeshAsset = (props: IAssetProps<any>): props is IAssetProps<AssetTypeEnum.MESH> => 
    !!(props as IAssetProps<AssetTypeEnum.MESH>).mesh;