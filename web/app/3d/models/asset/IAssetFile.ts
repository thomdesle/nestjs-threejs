import { AssetTypeEnum } from "./AssetTypeEnum";
import { GLTF } from "three/examples/jsm/loaders/GLTFLoader";
import { Mesh, Object3D } from "three";

export type IAssetFile<T extends AssetTypeEnum> = (
    T extends AssetTypeEnum.GLTF ? GLTF
    : T extends AssetTypeEnum.FBX ? Object3D
    : T extends AssetTypeEnum.MESH ? Mesh
    : never
)