export enum SlotTypeEnum {
    LEFT_HAND = 'LEFT_HAND',
    RIGHT_HAND = 'RIGHT_HAND'
}