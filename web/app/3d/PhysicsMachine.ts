import Ammo from "ammojs-typed";
import PhysicsBody from "./components/physics/PhysicsBody";

export class PhysicsMachine {
    public Ammo: typeof Ammo;
    private bodies: PhysicsBody[] = [];
    private world: Ammo.btDiscreteDynamicsWorld;

    constructor(_Ammo: typeof Ammo) {
        this.Ammo = _Ammo;
        const collisionConfiguration = new _Ammo.btDefaultCollisionConfiguration();
        const dispatcher = new _Ammo.btCollisionDispatcher(collisionConfiguration);
        const overlappingPairCache = new _Ammo.btDbvtBroadphase();
        const solver = new _Ammo.btSequentialImpulseConstraintSolver();
        this.world = new _Ammo.btDiscreteDynamicsWorld(dispatcher, overlappingPairCache, solver, collisionConfiguration);
        this.world?.setGravity(new _Ammo.btVector3(0, -75, 0));
    }

    public update = (delta: number) => {;
        this.world.stepSimulation(delta);
        this.bodies.map(body => body.update());
    }
    
    public register = (body: PhysicsBody) => {
        this.world.addRigidBody(body.body);
        this.bodies.push(body);
    }
}