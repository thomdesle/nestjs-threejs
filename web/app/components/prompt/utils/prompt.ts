import React from "react";
import { getAxis, Direction } from "../../../models/Direction";
import { getAvailableScreenSpace } from "../../../helpers/getAvailableScreenSpace";

export const getPromptPlacement = (origin: DOMRect, d?: Direction): React.CSSProperties => {
	let screenSpaceArr = getAvailableScreenSpace(origin);
	if (d) 
		screenSpaceArr.sort((a, b) => a.direction == d ? -1 : b.direction == d ? 1 : 0);

	const primaryAxis = getAxis(screenSpaceArr[0].direction);
	screenSpaceArr = [
		screenSpaceArr[0], 
		screenSpaceArr[screenSpaceArr.findIndex(o => getAxis(o.direction) !== primaryAxis)]
	];

	let placement = {};
	for (const screenSpace of screenSpaceArr) {
		const isPrimary = screenSpace === screenSpaceArr[0];
		const placementAttribute = {
			[Direction.LEFT]: { right: innerWidth - scrollX - (isPrimary ? origin.left : origin.right) },
			[Direction.RIGHT]: { left: scrollX + (isPrimary ? origin.right : origin.left) },
			[Direction.UP]: { bottom: innerHeight - scrollY - (isPrimary ? origin.top : origin.bottom) },
			[Direction.DOWN]: { top: scrollY + (isPrimary ? origin.bottom : origin.top) },
		}[screenSpace.direction];

		placement = {...placement, ...placementAttribute};
	}

	return placement;
}