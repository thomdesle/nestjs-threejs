import React, { CSSProperties, forwardRef, FunctionComponent, MutableRefObject, useEffect, useRef, useState } from "react";
import { createPortal } from "react-dom";
import styled from "styled-components";
import { Direction } from "../../models/Direction";
import { getPromptPlacement } from "./utils/prompt";

export type IPromptProps = {
	originRef?: MutableRefObject<any>,
	direction?: Direction
	children: React.ReactNode,
} & React.HTMLAttributes<HTMLDivElement>;

const PromptContainer = styled.div`
	position: absolute;
	display: block;
	outline: none;
`;

const Prompt: FunctionComponent<IPromptProps> = forwardRef<HTMLDivElement, IPromptProps>(({ children, direction, originRef: propsOriginRef, ...props }, forwardRef) => {
	const originRef: MutableRefObject<any> = useRef(null);
	const promptRef: MutableRefObject<any> = useRef(null);
	const [focusOrigin, setFocusOrigin] = useState<Element | null>();
	const [placement, setPlacement] = useState<CSSProperties>({});

	useEffect(() => {
		if (!promptRef.current)
			return;

		setFocusOrigin(document.activeElement);
		promptRef.current.focus();
	}, [promptRef.current])

	useEffect(() => {
		update();
	}, [originRef.current])

	const update = () => {
		if (!originRef.current)
			return;

		const parent = propsOriginRef?.current ?? originRef.current.parentNode;
		setPlacement(getPromptPlacement(parent.getBoundingClientRect(), direction));
	}

	useEffect(() => {
		return () => {
			if (promptRef.current?.contains(document.activeElement))
				(focusOrigin as HTMLElement)?.focus && (focusOrigin as HTMLElement).focus();
		}
	}, [])

	return (
		<>
			{<span ref={originRef} />}
			{createPortal((
				<PromptContainer ref={promptRef} style={placement} tabIndex={-1}>
					<div ref={forwardRef} {...props}>
						{children}
					</div>
				</PromptContainer>
			), document.body)}
		</>
	);
});

export default Prompt;