import React, { FunctionComponent } from "react";

type ISvgIconProps = {
	icon: string
}

export const SVG_ICONS_PATH = `/assets/icons/icons.svg`
const SvgIcon: FunctionComponent<ISvgIconProps> = ({ icon }) => {
	const path = `${SVG_ICONS_PATH}#${icon}`;
	return <svg> <use xlinkHref={path} href={path} /> </svg>
}

export default SvgIcon;