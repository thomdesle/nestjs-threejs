import React, { FunctionComponent, useEffect, useState } from "react"

type ISvgLoaderProps = {
	href: string;
}

const Svg: FunctionComponent<ISvgLoaderProps> = ({ href }) => {
	const [svg, setSvg] = useState<string>();

	useEffect(() => {
		fetch(href)
			.then(r => r.text())
			.then(setSvg);
	})

	return svg ? <div dangerouslySetInnerHTML={{ __html: svg }} /> : null
}

export default Svg