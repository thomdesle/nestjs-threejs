import React, { FunctionComponent, useMemo } from "react";
import styled, { css } from "styled-components";
import { randomColor } from "../../../helpers/colors/randomColor";
import { seed } from "../../../helpers/seed";


export type IUserAvatarProps = {
	userId: string;
	size?: number
}

const Container = styled.div<{ size: number, color: string }>`
	border-radius: 50%;
	overflow: hidden;
	position: relative;
	color: #FFF;
	text-align: center;

	${({ color }) => css`
		background-color: ${color};
	`}

	${({ size }) => css`
		width: ${size}px;
		height: ${size}px;
		line-height: ${size}px;
	`}

	img,
	svg,
	canvas {
		width: 66%;
		height: 66%;
		position: absolute;
		transform: translate(-50%, -50%);
		top: 50%;
		left: 50%;
	}
`;

export const UserAvatar: FunctionComponent<IUserAvatarProps> = ({ userId, size = 40 }) => {
	const [color] = useMemo(() => {
		const s = seed(userId)();
		return [randomColor(s)]
	}, [userId])

	return (
		<Container color={color} size={size}>
		</Container>
	);
}