import React, { FunctionComponent, useContext, useEffect, useState } from "react";
import styled, { createGlobalStyle, css } from "styled-components";
import { randomColor } from "../../helpers/colors/randomColor";
import { StoreContext } from "../../store/contexts/StoreContext";
import { IIntervalType } from "../../thirdparty/spotify/classes/IntervalManager";
import SpotifyListener from "../../thirdparty/spotify/classes/SpotifyListener";
import { IBar, IBeat, ISection, ISegment, ITatum } from "../../thirdparty/spotify/models/ISpotifyAnalysis";

const Speaker = styled.div<IIntervalType & { i: string }>`
	width: 60px;
	height: 60px;
	position: relative;
	display: inline-block;
	background: white;
	margin: 30px;
	
	${({ confidence, duration, i }) => css`
		animation: ease-out move-${i} .2s;
		@keyframes move-${i} {
			0% { 
				transform: scale(1);
			}
			10% {
				transform: translateX(${confidence * 100}px) scaleX(${confidence * 2}) scale(${1 + .5 * confidence});
			}
			100% {
				transform: scale(1);
			}
		}
	`}
`;

const GlobalStyle = createGlobalStyle<{ color: string, duration: number }>`
	html,
	body {
		transition: background-color ${({ duration }) => duration}s;
		background-color: ${({ color }) => color};
		color: #FFF;
	}
`;

export const Spotify: FunctionComponent<{}> = () => {
	const { sessionTokens: { spotifyAccessToken } } = useContext(StoreContext);

	const [bar, setBar] = useState<IBar>();
	const [beat, setBeat] = useState<IBeat>();
	const [tatum, setTatum] = useState<ITatum>();
	const [segment, setSegment] = useState<ISegment>();
	const [section, setSection] = useState<ISection>();
	const [style, setStyle] = useState({
		color: randomColor(),
		duration: .2
	});

	useEffect(() => {
		if (!spotifyAccessToken)
			return;

		const spotifyListener = new SpotifyListener(spotifyAccessToken);
		const { bars, sections, beats } = spotifyListener.intervalManagers;

		sections.listen(setSection);
		bars.listen(setBar);
		beats.listen(setBeat);
	}, [spotifyAccessToken]);

	useEffect(() => {
		if (bar)
			setStyle({
				duration: bar?.duration,
				color: randomColor()
			})
	}, [bar])

	return (
		<>
			<GlobalStyle {...style} />
			<div style={{ position: 'fixed', background: 'transparent', display: 'flex', flexDirection: 'column', justifyContent: 'space-between' }}>
				{segment &&
					<Speaker i={`${segment.start}`.replace('.', '')} {...segment} />}
				{tatum &&
					<Speaker i={`${tatum.start}`.replace('.', '')} {...tatum} />}
				{beat &&
					<Speaker i={`${beat.start}`.replace('.', '')} {...beat} />}
				{bar &&
					<Speaker i={`${bar.start}`.replace('.', '')} {...bar} />}
				{section &&
					<Speaker i={`${section.start}`.replace('.', '')} {...section} />}
			</div>
		</>
	);
}