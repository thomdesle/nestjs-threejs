export const randomColor = (random: number = Math.random()) => {
  return "#" + ("00000" + Math.floor(random * Math.pow(16, 6)).toString(16)).slice(-6);
}