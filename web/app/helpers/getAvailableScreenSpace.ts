import { Direction } from '../models/Direction';

export type IScreenSpace = {
    direction: Direction,
    space: number
}

const getAvailableScreenSpace = (rect: DOMRect, ...directions: Direction[]): IScreenSpace[] => {
    const { x, y, height, width } = rect;
    directions = directions.length > 0 
        ? directions 
        : [Direction.UP, Direction.DOWN, Direction.RIGHT, Direction.LEFT];

        const getSpace = (direction: Direction) => ({
            [Direction.UP]: y - scrollY,
            [Direction.DOWN]: scrollY + innerHeight - (y + height),
            [Direction.LEFT]: x - scrollX,
            [Direction.RIGHT]: scrollX + innerWidth - (x + width),
        })[direction]

    return directions.map(direction => ({
        direction,
        space: getSpace(direction)
    })).sort((a, b) => b.space - a.space);
}

const getRect = (element: HTMLElement) => {
    if (!element) return;
    const rect = element.getBoundingClientRect();
    return {
        left: scrollX + rect.left,
        right: scrollX + rect.right,
        top: scrollY + rect.top,
        bottom: scrollY + rect.bottom,
        x: scrollX + rect.x,
        y: scrollY + rect.y,
        height: rect.height,
        width: rect.width,
        toJSON: rect.toJSON
    }
}

export { getAvailableScreenSpace, getRect };