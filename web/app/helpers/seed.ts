// only works with mongoose ObjectId string
export const seed = (s: number | string) => {
	s = typeof s === 'number' ? s : parseInt(`${parseInt(s.valueOf(), 16)}`.replace('.', ''));
	
    let mask = 0xffffffff;
    let m_w  = (123456789 + s) & mask;
    let m_z  = (987654321 - s) & mask;

    return function() {
      m_z = (36969 * (m_z & 65535) + (m_z >>> 16)) & mask;
      m_w = (18000 * (m_w & 65535) + (m_w >>> 16)) & mask;

      let result = ((m_z << 16) + (m_w & 65535)) >>> 0;
      result /= 4294967296;
      return result;
    }
}