import React, { FunctionComponent } from "react";
import ReactDOM from "react-dom";
import { createGlobalStyle, ThemeProvider } from 'styled-components';
import { StoreContextProvider } from "./store/contexts/StoreContext";
import defaultTheme from "./styles/themes/defaultTheme";

const GlobalStyle = createGlobalStyle`
	html,
	body {
		margin: 0;
		font-family: 'Roboto', sans-serif;
		letter-spacing: .5px;
		transition: background-color 1s;
	}
`;

const App: FunctionComponent = ({ }) => {
	return (
		<StoreContextProvider>
			<ThemeProvider theme={defaultTheme}>
				<GlobalStyle />
			</ThemeProvider>
		</StoreContextProvider>
	)
}

ReactDOM.render(
	<App />,
	document.getElementById("root")
);