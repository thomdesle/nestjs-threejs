import { IBar, IBeat, ISection, ISegment, ITatum } from "../models/ISpotifyAnalysis";

export type IIntervalType = IBeat | IBar | ISegment | ISection | ITatum;

export default class Intervalhandler<T extends IIntervalType> {
	private arr: T[] = []
	private index = -1;
	private isPlaying = false;
	private listening: Array<(s: T) => void> = [];
	private listeningOnce: Array<(s: T) => void> = [];
	private lastSync = 0;

	private resync = () => {
		if (!this.isPlaying)
			return;

		this.index = this.arr.findIndex(({start: s, duration: d}) => s < this.lastSync && s + d > this.lastSync);
		if (this.index === -1)
			return;

		const segment = this.arr[this.index];
		const interval = (segment.duration - (this.lastSync - segment.start)) * 1000;

		setTimeout(() => {
			this.next(this.lastSync);
		}, interval);
	}

	private next = (time: number) => {
		if (!this.isPlaying ||
			this.lastSync !== time)
			return;

		this.index++;
		const segment = this.arr[this.index];
		if (!segment) 
			return;
		this.callback(segment);

		setTimeout(() => {
			this.next(time);
		}, segment.duration * 1000);
	}

	public listenOnce = (c: (s: T) => void) => {
		this.listeningOnce.push(c);
	}

	public listen = (c: (s: T) => void) => {
		this.listening.push(c);
	}

	private callback = (s: T) => {
		this.listening.map(c => c(s))
		this.listeningOnce.map(c => c(s));
		this.listeningOnce = [];
	}

	public sync = (time: number, arr?: T[]) => {
		this.lastSync = time;
		this.arr = arr ?? this.arr;
		this.resync();
	}

	public setPlaying = (isPlaying: boolean) => {
		this.isPlaying = isPlaying;
	}
}