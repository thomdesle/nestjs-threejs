import { IBar, IBeat, ISection, ISpotifyAnalysis, ITatum } from "../models/ISpotifyAnalysis";
import { ISpotifyTrack } from "../models/ISpotifyTrack";
import SpotifyService from "../services/SpotifyService";
import Intervalhandler from "./IntervalManager";

declare global {
	interface Window {
		SpotifyListener?: SpotifyListener
	}
}

export const SYNC_DELAY = 10000;

type IIntervalManagers = {
	bars: Intervalhandler<IBar>,
	beats: Intervalhandler<IBeat>,
	sections: Intervalhandler<ISection>,
	// segments: Intervalhandler<ISegment>
	// tatums: Intervalhandler<ITatum>,
}

export default class SpotifyListener {
	private service: SpotifyService;
	private track?: ISpotifyTrack;
	readonly intervalManagers: IIntervalManagers = {
		bars: new Intervalhandler<IBar>(),
		beats: new Intervalhandler<IBeat>(),
		sections: new Intervalhandler<ISection>(),
		// tatums: new Intervalhandler<ITatum>(),
		// segments: new Intervalhandler<ISegment>()
	}
	constructor(accessToken: string) {
		this.service = new SpotifyService(accessToken);
		this.sync();

		if (!window.SpotifyListener)
			window.SpotifyListener = this;
	}

	sync = () => {
		this.service.getCurrentlyPlaying().then(res => {
			if (!res.data) {
				this.track = undefined;
				this.setPlaying(false)
				return;
			}

			const { item, is_playing, progress_ms } = res.data;
			const playTime = progress_ms / 1000;
			this.setPlaying(is_playing);

			if (this.track?.id !== item.id)
				this.service.analyzeTrack(item.id).then(res => {
					if (!res.data)
						return;

					this.syncIntervalManagers(playTime, res.data);
				});
			else this.syncIntervalManagers(playTime);
			this.track = item;
		});

		setTimeout(() => {
			this.sync();
		}, SYNC_DELAY);
	}

	private syncIntervalManagers = (time: number, analysis?: ISpotifyAnalysis) => {
		Object.keys(this.intervalManagers).map((k) => {
			(this.intervalManagers as any)[k].sync(time, analysis ? (analysis as any)[k] : undefined);
		});
	}
	
	private setPlaying = (isPlaying: boolean) => {
		Object.keys(this.intervalManagers).map(k => {
			(this.intervalManagers as any)[k].setPlaying(isPlaying);
		});
	}
}