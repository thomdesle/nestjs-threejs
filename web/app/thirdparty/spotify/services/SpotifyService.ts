import axios from "axios"

export default class SpotifyService {
	private accessToken;
	constructor(accessToken: string) {
		this.accessToken = accessToken;
	}

	private config = () => ({
		headers: { Authorization: `Bearer ${this.accessToken}` }
	})

 	getCurrentlyPlaying = () => 
		axios.get('https://api.spotify.com/v1/me/player/currently-playing', this.config());

 	analyzeTrack = (trackId: string) => 
		axios.get(`https://api.spotify.com/v1/audio-analysis/${trackId}`, this.config());
}