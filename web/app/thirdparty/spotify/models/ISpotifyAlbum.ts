export type ISpotifyAlbum = {
	images: {
		height: number,
		width: number,
		url: string
	}[],
	name: string,
	release_date: string,
	type: string,
	uri: string,
	href: string
	id: string;
}