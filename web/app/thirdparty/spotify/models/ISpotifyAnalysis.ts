export type ISpotifyAnalysis = {
	bars: IBar[],
	beats: IBeat[],
	tatums: ITatum[]
	sections: ISection[],
	segments: ISegment[]
}

export type IBeat = {
	confidence: number;
	duration: number;
	start: number;
}

export type IBar = {
	confidence: number;
	duration: number;
	start: number;
}

export type ITatum = {
	confidence: number;
	duration: number;
	start: number;
}

export type ISegment = {
	pitches: number[],
	loudness_start: number,
	loudness_max_time: number,
	loudness_max: number,
	loudness_end: number
} & IBeat;

export type ISection = {
	key: number,
	key_confidence: number,
	loudness: number,
	mode: number,
	mode_confidence: number,
	tempo: number,
	tempo_confidence: number,
	time_signature: number,
	time_signature_confidence: number
} & IBeat;