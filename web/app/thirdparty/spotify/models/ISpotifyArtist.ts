export type ISpotifyArtist = {
	href: string;
	id: string;
	name: string;
	type: string;
	uri: string;
}