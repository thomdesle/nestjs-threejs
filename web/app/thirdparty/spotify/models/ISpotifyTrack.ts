import { ISpotifyAlbum } from "./ISpotifyAlbum";
import { ISpotifyArtist } from "./ISpotifyArtist";

export type ISpotifyTrack = {
	uri: string;
	preview_url: string;
	duration_ms: number;
	name: string;
	artists: ISpotifyArtist[];
	href: string;
	album: ISpotifyAlbum;
	id: string;
}