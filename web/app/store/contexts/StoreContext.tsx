import axios from "axios";
import React, { FunctionComponent, useEffect, useState } from "react";
import { ISessionTokens, ISessionUser } from '../../models/user/ISessionUser';
import SpotifyListener from "../../thirdparty/spotify/classes/SpotifyListener";

type IStoreContext = {
	sessionUser?: ISessionUser,
	sessionTokens: ISessionTokens,
	store: (payload: Partial<IStoreContext>) => void
}

const defaultStoreContext: IStoreContext = {
	sessionTokens: {},
	store: () => { }
}

export const StoreContext = React.createContext(defaultStoreContext);

export const StoreContextProvider: FunctionComponent = ({ children }) => {
	const [state, setState] = useState<Omit<IStoreContext, 'store'>>(defaultStoreContext);

	useEffect(() => {
		axios.get('/api/users/current').then(res => {
			if (!res.data)
				return;
			store({ sessionUser: res.data })

			axios.get('/api/auth/tokens').then(res => {
				if (!res.data)
					return;
				store({ sessionTokens: res.data })

				const { spotifyAccessToken } = res.data;
				if (spotifyAccessToken)
					new SpotifyListener(spotifyAccessToken);

				// ready for 3dapp!
				require('../../3d/3dapp');
			});
		});
	}, [])

	const store = (payload: Partial<IStoreContext>) => {
		setState({
			...state,
			...payload
		});
	}

	return (
		<StoreContext.Provider value={{ ...state, store }}>
			{children}
		</StoreContext.Provider>
	);
}