export enum Direction {
	UP = 'up',
	DOWN = 'down',
	LEFT = 'left',
	RIGHT = 'right'
}


export enum Axis {
	X = 'x',
	Y = 'y'
}

export const getAxis = (d: Direction) => d === Direction.DOWN || d === Direction.UP ? Axis.Y : Axis.X;