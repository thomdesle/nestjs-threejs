import { FileTypeEnum } from "./FileTypeEnum";
import { FileExtensionEnum } from './FileExtensionEnum';

export type IFile = {
    _id: string
    path: string
    name: string
    title: string
    extension: FileExtensionEnum
    type: FileTypeEnum
}