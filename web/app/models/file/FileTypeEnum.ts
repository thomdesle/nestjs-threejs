export enum FileTypeEnum {
    AUDIO = 'audio',
    APPLICATION = 'application',
    VIDEO = 'video',
    IMAGE = 'image',
    TEXT = 'text',
    FONT = 'font'
}