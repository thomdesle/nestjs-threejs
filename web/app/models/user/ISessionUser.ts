import { Role } from './roles/Role';

export type ISessionUser = {
	_id: string;
	email: string;
	roles: Role[];
	isVerified: boolean;
	googleId?: string;
	spotifyId?: string;
}

export type ISessionTokens = {
	googleAccessToken?: string;
	spotifyAccessToken?: string;
}