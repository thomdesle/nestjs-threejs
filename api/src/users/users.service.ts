import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { FilterQuery, Model, Types } from 'mongoose';
import { CreateUserDto } from './dto/CreateUserDto';
import { User, UserDocument } from './users.schema';
import * as bcrypt from 'bcrypt';
import { UpdateUserDto } from './dto/UpdateUserDto';

@Injectable()
export class UsersService {
  constructor(@InjectModel(User.name) private userModel: Model<UserDocument>) {}

  async create(createUserDto: CreateUserDto): Promise<User> {
    const user = new this.userModel(createUserDto);
    if (user.password) user.password = await bcrypt.hash(user.password, 10);
    return user.save();
  }

  findById(id: Types.ObjectId): Promise<User> {
    return this.userModel.findById(id).exec();
  }

  findOne(query: FilterQuery<UserDocument>): Promise<User> {
    return this.userModel.findOne(query).exec();
  }

  findAll(): Promise<User[]> {
    return this.userModel.find().exec();
  }

  deleteOne(id: Types.ObjectId) {
    return this.userModel.deleteOne(id).exec();
  }

  updateOne(
    _id: UserDocument['_id'],
    updateUserDto: UpdateUserDto,
  ): Promise<User> {
    return this.userModel
      .updateOne({ _id }, updateUserDto, { new: true })
      .exec() as any;
  }
}
