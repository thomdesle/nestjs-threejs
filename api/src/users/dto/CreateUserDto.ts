export class CreateUserDto {
  email: string;
  displayName?: string;
  isVerified?: boolean;
  googleId?: string;
  spotifyId?: string;
  password?: string;
}
