import {
  BadRequestException,
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  Request,
  UseInterceptors,
} from '@nestjs/common';
import { Role } from 'models/users/Role';
import { SanitizeMongooseModelInterceptor } from 'nestjs-mongoose-exclude';
import { Roles } from 'src/common/guards/RolesGuard';
import { Public, SessionUser } from 'src/common/guards/SessionGuard';
import { CreateUserDto } from './dto/CreateUserDto';
import { UpdateUserDto } from './dto/UpdateUserDto';
import { User } from './users.schema';
import { UsersService } from './users.service';
import { MailerService } from '../mailer/mailer.service';
import { SpotifyAccessToken } from 'src/common/guards/SpotifyAuthGuard';
import { GoogleAccessToken } from 'src/common/guards/GoogleAuthGuard';

@UseInterceptors(
  new SanitizeMongooseModelInterceptor({
    excludeMongooseId: false,
    excludeMongooseV: true,
  }),
)
@Controller('users')
export class UsersController {
  constructor(private readonly usersService: UsersService, private readonly mailerService: MailerService) {}

  @Post('register')
  @Public()
  async register(@Body() { email, password }: CreateUserDto): Promise<User> {
    try {
      const user = await this.usersService.create({ email, password });
	  return user;
    } catch (e) {
      return new BadRequestException(e) as any;
    }
  }

  @Get('current')
  async getCurrentUser(@SessionUser() user, @SpotifyAccessToken() spotifyAccessToken, @GoogleAccessToken() googleAccessToken): Promise<any> {
    return user;
  }

  @Patch('current')
  async updateCurrentUser(
    @SessionUser() user,
    @Body() { displayName }: UpdateUserDto,
  ) {
    try {
      return await this.usersService.updateOne(user._id, { displayName });
    } catch (e) {
      return new BadRequestException(e);
    }
  }

  @Get(':id')
  @Roles(Role.ADMIN)
  async getUser(@Param('id') _id) {
    try {
      return await this.usersService.findById(_id);
    } catch (e) {
      return new BadRequestException(e);
    }
  }
}
