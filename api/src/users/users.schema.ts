import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Role } from 'models/users/Role';
import { Document } from 'mongoose';
import { ExcludeProperty } from 'nestjs-mongoose-exclude';

export type UserDocument = User & Document;

@Schema()
export class User {
  @Prop({ required: true, unique: true })
  email: string;

  @Prop({})
  displayName: string;

  @Prop({ unique: true })
  googleId: string;

  @Prop({ unique: true })
  spotifyId: string;

  @ExcludeProperty()
  @Prop({})
  password: string;

  @Prop({ default: false })
  isVerified: boolean;

  @Prop({ default: [Role.USER] })
  roles: Role[];
}

export const UserSchema = SchemaFactory.createForClass(User);
