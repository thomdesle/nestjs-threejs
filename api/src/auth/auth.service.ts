import { Injectable } from '@nestjs/common';
import { UsersService } from '../users/users.service';
import { Profile as GoogleProfile } from 'passport-google-oauth20';
import { Profile as SpotifyProfile } from 'passport-spotify';
import * as bcrypt from 'bcrypt';
import { User } from 'src/users/users.schema';

@Injectable()
export class AuthService {
  constructor(private usersService: UsersService) {}

  async validatePassword(email: string, password: string) {
    const user = await this.usersService.findOne({ email });
    if (!user) return null;

    const isMatch = await bcrypt.compare(password, user.password);
    if (!isMatch) return null;

    return user;
  }

  async validateSpotify(profile: SpotifyProfile): Promise<null | User> {
	  try {
		const user = await this.usersService.findOne({ spotifyId: profile.id });
		if (user) return user;

		const newUser = await this.usersService.create({
			email: profile.emails[0].value,
			spotifyId: profile.id,
			isVerified: true
		});

		return newUser;
	  } catch (e) {
		  return null;
	  }
  }

  async validateGoogle(profile: GoogleProfile): Promise<null | User> {
    try {
      const user = await this.usersService.findOne({ googleId: profile.id });
      if (user) return user;

      const newUser = await this.usersService.create({
        email: profile.emails[0].value,
        googleId: profile.id,
        isVerified: !!profile.emails[0].verified,
      });

      return newUser;
    } catch (e) {
      return null;
    }
  }
}
