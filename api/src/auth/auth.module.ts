import { Module } from '@nestjs/common';
import { PassportModule } from '@nestjs/passport';
import { UsersModule } from 'src/users/users.module';
import { AuthService } from './auth.service';
import { LocalStrategy } from './strategies/local.stragey';
import { AuthController } from './auth.controller';
import { GoogleStrategy } from './strategies/google.strategy';
import { SpotifyStrategy } from './strategies/spotify.strategy';
import { APP_GUARD } from '@nestjs/core';
import { SessionGuard } from 'src/common/guards/SessionGuard';
import { RolesGuard } from 'src/common/guards/RolesGuard';
import { SessionSerializer } from './serializers/SessionSerializer';

@Module({
  imports: [
    UsersModule,
    PassportModule.register({
      session: true,
    }),
  ],
  providers: [
    AuthService,
    LocalStrategy,
    GoogleStrategy,
	SpotifyStrategy,
    {
      provide: APP_GUARD,
      useClass: SessionGuard,
    },
    {
      provide: APP_GUARD,
      useClass: RolesGuard,
    },
    SessionSerializer,
  ],
  controllers: [AuthController],
})
export class AuthModule {}
