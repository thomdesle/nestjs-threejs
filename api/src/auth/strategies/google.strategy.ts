import { PassportStrategy } from '@nestjs/passport';
import { Injectable } from '@nestjs/common';
import { Profile, Strategy } from 'passport-google-oauth20';
import { AuthService } from '../auth.service';
import { User } from 'src/users/users.schema';
import { GOOGLE_SESSION_TOKEN_KEY } from 'src/common/guards/GoogleAuthGuard';

@Injectable()
export class GoogleStrategy extends PassportStrategy(Strategy, 'google') {
  constructor(private authService: AuthService) {
    super({
      clientID: process.env.GOOGLE_CLIENT_ID,
      clientSecret: process.env.GOOGLE_SECRET,
      callbackURL: `${process.env.SERVER_URL}/auth/google/redirect`,
	  passReqToCallback: true,
      showDialog: true,
      scope: ['email']
    });
  }

  async validate(
	req: any,
    accessToken: string,
    refreshToken: string,
    profile: Profile,
  ): Promise<User | null> {
	req.session[GOOGLE_SESSION_TOKEN_KEY] = accessToken;
    return await this.authService.validateGoogle(profile);
  }
}
