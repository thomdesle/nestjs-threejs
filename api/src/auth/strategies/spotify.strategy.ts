import { PassportStrategy } from '@nestjs/passport';
import { Injectable } from '@nestjs/common';
import { AuthService } from '../auth.service';
import { User } from 'src/users/users.schema';
import { Strategy, Profile } from 'passport-spotify';
import { SPOTIFY_SESSION_TOKEN_KEY } from 'src/common/guards/SpotifyAuthGuard';

@Injectable()
export class SpotifyStrategy extends PassportStrategy(Strategy, 'spotify') {
  constructor(private authService: AuthService) {
    super({
      clientID: process.env.SPOTIFY_CLIENT_ID,
      clientSecret: process.env.SPOTIFY_SECRET,
      callbackURL: `${process.env.SERVER_URL}/auth/spotify/redirect`,
	  passReqToCallback: true,
      showDialog: true,
      scope: ['user-read-email', 'user-read-currently-playing', 'user-read-playback-state', 'user-modify-playback-state']
    });
  }

  async validate(
	req: any,
    accessToken: string,
    refreshToken: string,
    profile: Profile,
    done,
  ): Promise<User | null> {
	req.session[SPOTIFY_SESSION_TOKEN_KEY] = accessToken;
    return await this.authService.validateSpotify(profile);
  }
}
