import { PassportSerializer } from '@nestjs/passport';
import { Injectable } from '@nestjs/common';
import { UserDocument } from 'src/users/users.schema';
import { UsersService } from 'src/users/users.service';

@Injectable()
export class SessionSerializer extends PassportSerializer {
  constructor(private usersService: UsersService) {
    super();
  }

  serializeUser(user: UserDocument, done: (err: any, id?: any) => void): void {
    done(null, user?._id);
  }

  deserializeUser(
    id: UserDocument['_id'],
    done: (err: any, id?: any) => void,
  ): void {
    this.usersService
      .findById(id)
      .then((user) => {
        done(null, user);
      })
      .catch((e) => {
        done(e, null);
      });
  }
}
