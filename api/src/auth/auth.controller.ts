import { Controller, Get, Post, Response, UseGuards } from '@nestjs/common';
import { GoogleAccessToken, GoogleAuthGuard } from 'src/common/guards/GoogleAuthGuard';
import { Public } from 'src/common/guards/SessionGuard';
import { LocalAuthGuard } from 'src/common/guards/LocalAuthGuard';

import { AuthService } from './auth.service';
import { SpotifyAccessToken, SpotifyAuthGuard } from 'src/common/guards/SpotifyAuthGuard';

@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @UseGuards(LocalAuthGuard)
  @Post()
  localAuth(@Response() res) {
	  return res.redirect(process.env.APP_URL);
  }

  @Public()
  @UseGuards(GoogleAuthGuard)
  @Get('google')
  googleAuth() {
    // initiates redirect to google login screen
  }

  @Public()
  @UseGuards(GoogleAuthGuard)
  @Get('google/redirect')
  async googleAuthRedirect(@Response() res) {
	  return res.redirect(process.env.APP_URL);
  }

  @Public()
  @UseGuards(SpotifyAuthGuard)
  @Get('spotify')
  spotifyAuth() {
    // initiates redirect to google login screen
  }

  @Public()
  @UseGuards(SpotifyAuthGuard)
  @Get('spotify/redirect')
  async spotifyAuthRedirect(@Response() res) {
	  return res.redirect(process.env.APP_URL);
  }

  @Get('tokens')
  async getAccessTokens( @GoogleAccessToken() googleAccessToken, @SpotifyAccessToken() spotifyAccessToken ) {
	  return {
		  googleAccessToken,
		  spotifyAccessToken
	  };
  }
}
