import { Injectable } from '@nestjs/common';
import { ISendMailOptions, MailerService as MS } from '@nestjs-modules/mailer';

@Injectable()
export class MailerService {
	constructor(private readonly mailerServce: MS) {}

	sendMail(options: ISendMailOptions): Promise<any> {
		return this.mailerServce.sendMail(options);
	}
}
