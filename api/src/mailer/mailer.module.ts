import { Module } from '@nestjs/common';
import { MailerModule as MM } from '@nestjs-modules/mailer';
import { EjsAdapter } from '@nestjs-modules/mailer/dist/adapters/ejs.adapter';
import { MailerService } from './mailer.service';
import { ConfigModule } from '@nestjs/config';

@Module({
	imports: [
    ConfigModule.forRoot({
      envFilePath: '../.dev.env',
    }),
		MM.forRoot({
			// transport: process.env.MAIL_TRANSPORT,
			transport: {
				host: 'localhost',
				port: 1025,
				ignoreTLS: true,
				secure: false
			},
			defaults: {
				from: `${process.env.MAIL_FROM_NAME} <${process.env.MAIL_FROM}>`,
			},
      		preview: true,
		}),
	],
 	providers: [MailerService],
  	exports: [MailerService],
})
export class MailerModule {}
